CREATE USER "home-finances";
ALTER USER "home-finances" WITH PASSWORD 'home-finances';
CREATE DATABASE "home-finances";
GRANT ALL PRIVILEGES ON DATABASE "home-finances" TO "home-finances";
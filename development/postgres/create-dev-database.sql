CREATE USER "home-finances-dev";
ALTER USER "home-finances-dev" WITH PASSWORD 'home-finances-dev';
CREATE DATABASE "home-finances-dev";
GRANT ALL PRIVILEGES ON DATABASE "home-finances-dev" TO "home-finances-dev";
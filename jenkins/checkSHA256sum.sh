#!/bin/sh

if [ $# -lt 2 ]
  then
    echo "Invalid arguments supplied. Usage checkSHA256Sum <checksum> <file>"
    return 1;
fi

CHECKSUM=$1
FILE=$2

CALCULATED_CHECKSUM=$(sha256sum  "$FILE" | awk '{print $1}')

if [ "$CALCULATED_CHECKSUM" != "$CHECKSUM" ]
  then
    echo "Checksums do not match"
    return 1;
fi

echo "Checksums match"

return 0;